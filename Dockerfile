FROM registry.gitlab.com/jcgruenhage/docker-base-caddy
ENV UID=1337 \
    GID=1337
RUN apk add --no-cache \
      su-exec \
      s6 \
      ca-certificates \
      bash
ADD root /
EXPOSE 2015 80 443
VOLUME ["/etc/caddy", "/var/www"]
CMD ["/bin/s6-svscan", "/etc/s6.d/"]
