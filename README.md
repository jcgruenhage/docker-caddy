## Container Image for Caddy

#### Build-time variables
 - **CLONE_URL**: From where to clone caddy (default: https://github.com/mholt/caddy.git)
 - **BRANCH**: Which branch to use. (default: `latest stable tag)

#### Environment variables
 - **UID**: user id (default: 1337)
 - **GID**: group id (default: 1337)
 - **DOMAIN**: the domain that should be served (required for automatic tls)
 - **EMAIL**: the email address to use for let's encrypt (required for automatic tls)
 - **USE_QUIC**: set this to anything to enable experimental quic support (default: off)
 - **LE_STAGING**: use staging let's encrypt endpoint (default: off -> production)

#### Volumes
 - **/etc/caddy/Caddyfile**: webserver configuration (optional)
 - **/var/www/**: content to serve
 - **/caddy**: caddy will store it's certificates here (recommended if tls is active)

#### Ports
 - 80
 - 443

#### Basic docker-compose.yml example
Insecure example serving the contents of `./website`on port 80:
```yaml
version: '2'

services:
    caddy:
        image: registry.gitlab.com/jcgruenhage/docker-caddy
        container_name: caddy
        ports:
            - 80:80
        volumes:
            - ./website:/var/www/
```

Secure example serving the contents of `./website`on https://example.com/ with an automatic redirect to https:
```yaml
version: '2'

services:
    caddy:
        image: registry.gitlab.com/jcgruenhage/docker-caddy
        container_name: caddy
        ports:
            - 80:80
            - 443:443
        volumes:
            - ./website:/var/www/
        environment:
            - DOMAIN=example.com
            - EMAIL=you@example.com
```

Custom example doing whatever you want (take a look at https://caddyserver.com/docs):
```yaml
version: '2'

services:
    caddy:
        image: registry.gitlab.com/jcgruenhage/docker-caddy
        container_name: caddy
        ports:
            - 80:80
            - 443:443
        volumes:
            - ./website:/var/www
            - ./Caddyfile:/etc/Caddyfile
```
